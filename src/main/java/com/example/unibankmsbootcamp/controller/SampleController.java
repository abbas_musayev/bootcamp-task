package com.example.unibankmsbootcamp.controller;

import com.example.unibankmsbootcamp.entity.Student;
import com.example.unibankmsbootcamp.service.StudentService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class SampleController {

    private final StudentService studentService;

    @PostMapping
    public String saveStudent(@RequestBody Student request){
        return studentService.saveStudent(request);
    }


}

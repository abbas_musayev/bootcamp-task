package com.example.unibankmsbootcamp.service.impl;

import com.example.unibankmsbootcamp.entity.Student;
import com.example.unibankmsbootcamp.repo.StudentRepo;
import com.example.unibankmsbootcamp.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {


    private final StudentRepo repo;

    @Override
    public String saveStudent(Student request) {
        Student save = repo.save(request);
        System.out.println(save);
        return "Saved";
    }
}

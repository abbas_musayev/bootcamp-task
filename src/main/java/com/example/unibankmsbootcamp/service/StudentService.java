package com.example.unibankmsbootcamp.service;

import com.example.unibankmsbootcamp.entity.Student;

public interface StudentService {

    String saveStudent(Student request);
}

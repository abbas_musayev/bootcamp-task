package com.example.unibankmsbootcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnibankMsBootcampApplication {

    public static void main(String[] args) {
        SpringApplication.run(UnibankMsBootcampApplication.class, args);
    }
}
